// var yearElement = document.querySelector('.timer-year')
var daysElement = document.querySelector('.timer-days')
var hoursElement = document.querySelector('.timer-hours')
var minutesElement = document.querySelector('.timer-minutes')
var secondsElement = document.querySelector('.timer-seconds')

setInterval(function () {
    var miseconds = (new Date() - new Date(2018, 12, 18, 0, 0))
    // yearElement.textContent = Math.trunc(miseconds / 60000 / 60 / 24 / 365)
    daysElement.textContent = Math.trunc(miseconds / 60000 / 60 / 24)
    hoursElement.textContent = Math.trunc(miseconds / 60000 / 60 % 24)
    minutesElement.textContent = Math.trunc(miseconds / 60000 % 60)
    secondsElement.textContent = Math.trunc(miseconds / 1000 % 60)
}, 1000)

