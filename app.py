from flask import Flask, render_template

app = Flask(__name__)


@app.route('/')
def home():
    return render_template(["index.html", "main.css", "main.js"])

@app.route('/about')
def about():
    return render_template(["about.html", "main.css", "main.js"])


if __name__ == '__main__':
    app.run(host='0.0.0.0')
    # app.run(host="127.0.0.1")
